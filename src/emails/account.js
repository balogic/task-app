const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const sendWelcomeEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'rbalajives@gmail.com',
        subject: 'Thanks for Joining Us',
        text: `Hi, ${name}, Welcome to Task Manager App`
    })
}

// Send Email after Account Cancellation
const sendCancellationEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'rbalajives@gmail.com',
        subject: 'Sorry to See you Go!',
        text: `Hi ${name}, May we know why you have left us?`
    })
}

module.exports = {
    sendWelcomeEmail,
    sendCancellationEmail
}