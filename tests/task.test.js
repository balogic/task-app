const request = require('supertest')
const Task = require('../src/models/task')
const app = require('../src/app')

const {
    userOne,
    userOneId,
    populateDatabase
} = require('./fixtures/db')

beforeEach(populateDatabase)

test('Should create a Task', async () => {
    const response = await request(app).post('/tasks')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            "description": "Download Movies",
            "isCompleted": false
        })
        .expect(200)
    const task = await Task.findById(response.body._id)
    expect(task).not.toBeNull
    expect(task.isCompleted).toEqual(false)
    expect(task.owner).toEqual(userOne._id)
})

test('Should not create task without User', async () => [
    await request(app).post('/tasks')
    .send({
        "description": "Download Movies",
        "isCompleted": false
    })
    .expect(401)
])