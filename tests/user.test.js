const request = require('supertest')
const app = require('../src/app')
const User = require('../src/models/user')
const {
    userOne,
    userOneId,
    populateDatabase
} = require('./fixtures/db')

beforeEach(populateDatabase)

// Test for SignUp
test('Should Sign Up a New User', async () => {
    const response = await request(app)
        .post('/users')
        .send({
            name: "Balaji",
            email: "rbalajives@gmail.com",
            password: "Balaji123#"
        })
        .expect(201)

    const user = await User.findById(response.body.user._id)
    expect(user).not.toBeNull()
    expect(response.body).toMatchObject({
        user: {
            name: "Balaji",
            email: "rbalajives@gmail.com"
        },
        token: user.tokens[0].token
    })

})

// Test Login
test('Should Login', async () => {
    const response = await request(app)
        .post('/users/login')
        .send({
            email: userOne.email,
            password: userOne.password
        })
        .expect(200)
    const user = await User.findById(userOneId)
    expect(response.body.token).toBe(user.tokens[0].token)
})

test('Should not login with invalid user', async () => {
    await request(app)
        .post('/users/login')
        .send({
            email: 'john@gmail.com',
            password: "Balaji123#"
        })
        .expect(400)
})

// Test Authentication
test('Should get the User Profile', async () => {
    await request(app)
        .get('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
})

test('Should not get the User Profile', async () => {
    await request(app)
        .get('/users/me')
        .send()
        .expect(401)
})


// Test Deletion
test('Should delete the account', async () => {
    await request(app)
        .delete('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
    const user = await User.findById(userOneId)
    expect(user).toBeNull()
})

test('Should not delete the account', async () => {
    await request(app)
        .delete('/users/me')
        .send()
        .expect(401)
})

test('Should upload an Avatar', async () => {
    await request(app).post('/users/me/avatar')
        .set('Authorization', userOne.tokens[0].token)
        .attach('avatars', 'tests/fixtures/profile.png')
        .expect(200)
    const user = await User.findById(userOne._id)
    expect(user.avatar).toEqual(expect.any(Buffer))
})